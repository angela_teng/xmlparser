﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace xmlParser
{
    class Program
    {
        static void Main(string[] args)
        {

            String dirname = @"D:\Documents\LOD\聯合目錄XML檔\民俗戲劇\04_XueShuSheHui_TNUA_LiBoJun\";
         //string savedirname = @"D:\Documents\LOD\生物-魚類\04_Backup2\已處理\";
         string[] lines = System.IO.File.ReadAllLines(dirname + "04_FileList.txt");
         //GetMetaColon(dirname, lines, "07");
         GetProjName(dirname, lines, "04");
         GetMetaModel(dirname, lines, "04");
         //GetMetaModel_Colon(dirname, lines, "02");
         //GetOriginalValue(dirname, lines, "02");
         //CustomFixField(dirname, lines, savedirname);


         Console.WriteLine("按任意鍵繼續");
         Console.ReadKey(true);
            
        }

        static void CustomFixField(String dirname, string[] lines, String savedirname)
        {
            string fieldvalue = "學名";

            foreach (string line in lines) {

                XDocument docNew = XDocument.Load(dirname + line);
                IEnumerable<XElement> elements =
                        docNew.Element("DACatalog").Elements("MetaDesc").Elements("Title");

                foreach (XElement node in elements) {
                    if (node.Value.StartsWith(fieldvalue)) {
                           string newValue = node.Value.Substring(node.Value.IndexOf("：") + 1);
                           node.SetValue(newValue);
                           node.SetAttributeValue("field", fieldvalue);
                           Console.WriteLine(newValue);
                    }
                    docNew.Save(savedirname + line);
                }

            }
        
        }



        static void GetOriginalValue(String dirname, string[] lines, string num)
        {
            //02
            List<string> attrList = new List<string>() { "經緯度" };
             string logname = num + "_OriginalData.txt";
             using (System.IO.StreamWriter file =
                         new System.IO.StreamWriter(@"D:\Documents\LOD\10503\" + logname))
             {

                 foreach (string line in lines)
                 {
                     XDocument docNew = XDocument.Load(dirname + line);
                     IEnumerable<XElement> elements =
                             docNew.Element("DACatalog").Elements("MetaDesc").Descendants();


                     foreach (XElement node in elements)
                     {
                         IEnumerable<XAttribute> attList = node.Attributes();
                         if (attList.Count() > 0) { continue; }
                         else
                         {
                             string content = node.Value.ToString();
                             content = content.Replace("：", ":");
                             int pos;
                             pos = content.IndexOf(':');
                             if (pos == -1) { continue; }
                             string a = content.Substring(0, pos);

                             foreach (string n in attrList)
                             {
                                 if (n.Equals(a))
                                 {
                                     Console.WriteLine(node.Value);
                                     file.WriteLine(node.Value);
                                 }
                             }
                         }
                     }
                 }
             }
        }


        static void GetMetaModel_Colon(String dirname, string[] lines, string num)
        {
            List<string> dataModel = new List<string>();
            string logname = num + "_DataModel.txt";
            using (System.IO.StreamWriter file =
                        new System.IO.StreamWriter(@"D:\Documents\LOD\10503\" + logname))
            {
                foreach (string line in lines)
                {
                    XDocument docNew = XDocument.Load(dirname + line);
                    IEnumerable<XElement> elements =
                            docNew.Element("DACatalog").Elements("MetaDesc").Descendants();


                    foreach (XElement node in elements)
                    {
                        string name = node.Name.ToString().Trim();
                        string content = node.Value.ToString().Trim();
                        List<string> temp = new List<string>();

                       
                            content = content.Replace("：", ":");
                            int pos;
                            pos = content.IndexOf(':');
                            
                        if(pos!=-1){
                            string a = content.Substring(0, pos);
                            temp.Add(a.Trim());
                            //content = content.Remove(0, pos + 1);
                            //Console.WriteLine(a.Trim());
                            node.SetAttributeValue("Myfield", a.Trim());
                        }
                            
                       

                        IEnumerable<XAttribute> attList = node.Attributes();
                        string value;
                        if (attList.Count() > 0)
                        {
                            foreach (XAttribute a in attList)
                            {
                                value = name + " " + a.ToString().Trim();
                                Console.WriteLine(value);
                                file.WriteLine(value);

                            }
                        }
                        else
                        {
                            value = name;
                            Console.WriteLine(value);
                            file.WriteLine(value);
                        }
                    }
                }
              }
            }





        static void GetMetaModel(String dirname, string[] lines, string num)
        {
             //List<string> dataModel = new List<string>();
             string logname = num + "_DataModel.txt";
             using (System.IO.StreamWriter file =
                         new System.IO.StreamWriter(@"D:\Documents\LOD\聯合目錄XML檔\民俗戲劇\" + logname))
             { 
                foreach (string line in lines)
                {
                    XDocument docNew = XDocument.Load(dirname + line);
                    IEnumerable<XElement> elements =
                            docNew.Element("DACatalog").Elements("MetaDesc").Descendants();
                 
                    foreach (XElement node in elements)
                    {
                       string name = node.Name.ToString().Trim();
                       IEnumerable<XAttribute> attList = node.Attributes();
                       string value;
                  
                        if (attList.Count() > 0)
                       {
                           foreach (XAttribute a in attList)
                           {
                               value = line + "," + name + " " + a.ToString().Trim();
                               Console.WriteLine(value);
                               file.WriteLine(value);                       
                           }
                       }
                       else
                       {
                           value = line + "," + name;
                           //value =  name;
                           Console.WriteLine(value);
                           file.WriteLine(value);
                       }
                    }               
                }
         }
        }




        static void GetProjName(String dirname, string[] lines, string num) {

            string logname = num + "_ProjName.txt";
            using (System.IO.StreamWriter file =
                        new System.IO.StreamWriter(@"D:\Documents\LOD\聯合目錄XML檔\民俗戲劇\" + logname))
            {

                foreach (string line in lines)
                {
                    XDocument docNew = XDocument.Load(dirname + line);
                    IEnumerable<XElement> elements =
                            docNew.Element("DACatalog").Elements("AdminDesc").Elements("Catalog").Elements("Record");
                    //Dictionary<string, string> dicProj =
                    //    new Dictionary<string, string>();

                    foreach (XElement node in elements)
                    {
                        Console.WriteLine(line + "," + node.Value);
                        file.WriteLine(line + "," + node.Value);
                    }
                }
            }
     
        }

        static void GetMetaColon(String dirname, string[] lines, string num)
        {
            string logname = num + "_field.txt";
            using (System.IO.StreamWriter file =
                      new System.IO.StreamWriter(@"D:\Documents\LOD\10503\" + logname))
            {
                foreach (string line in lines)
                {
                    XDocument docNew = XDocument.Load(dirname + line);

                    IEnumerable<XElement> elements =
                    docNew.Element("DACatalog").Elements("MetaDesc");
                    elements =
                        from el in elements.Descendants()
                        where (el.Value.Contains(":") || el.Value.Contains("：")) && (!el.Value.Contains("http:"))
                        select el;
                    int n = 0;
                    foreach (XElement node in elements)
                    {
                        n += 1;
                    }

                    if (n > 0)
                    {
                        file.WriteLine(line + ":" + n);
                    }
                }
            }
        }


    }
}
